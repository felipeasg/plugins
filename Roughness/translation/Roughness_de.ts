<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="de">
<context>
    <name>QObject</name>
    <message>
        <location filename="../../../../../../Build/itom/SDK/include/common/helperCommon.cpp" line="+50"/>
        <source>parameter vector is not initialized</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+21"/>
        <source>mandatory parameter vector is not initialized</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>optional parameter vector is not initialized</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>output parameter vector is not initialized</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+32"/>
        <location line="+21"/>
        <source>parameter &apos;%1&apos; cannot be found in given parameter vector</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+78"/>
        <location line="+123"/>
        <source>name of requested parameter is empty.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-116"/>
        <location line="+123"/>
        <source>the parameter name &apos;%1&apos; in invald</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-99"/>
        <source>array index of parameter out of bounds.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+125"/>
        <source>given index of parameter name ignored since parameter is no array type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-118"/>
        <location line="+125"/>
        <source>parameter not found in m_params.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-14"/>
        <source>array index out of bounds.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+55"/>
        <source>invalid parameter name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../viewPlanning.cpp" line="+63"/>
        <source>Algorithms for view planning based on polygonial meshed model</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>-</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ito::AddInActuator</name>
    <message>
        <location filename="../../../../../../Build/itom/SDK/include/common/addInInterface.cpp" line="+683"/>
        <source>Constructor must be overwritten</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Destructor must be overwritten</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ito::AddInAlgo</name>
    <message>
        <location line="+92"/>
        <source>Constructor must be overwritten</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../../../../Build/itom/SDK/include/common/addInInterface.h" line="+985"/>
        <source>uninitialized vector for mandatory parameters!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>uninitialized vector for optional parameters!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>uninitialized vector for output parameters!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ito::AddInBase</name>
    <message>
        <location filename="../../../../../../Build/itom/SDK/include/common/addInInterface.cpp" line="-577"/>
        <source>function execution unused in this plugin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Toolbox</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+147"/>
        <source>Your plugin is supposed to have a configuration dialog, but you did not implement the showConfDialog-method</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ito::AddInDataIO</name>
    <message>
        <location line="+13"/>
        <source>Constructor must be overwritten</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Destructor must be overwritten</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+15"/>
        <source>listener does not have a slot </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>this object already has been registered as listener</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+12"/>
        <source>timer could not be set</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+25"/>
        <source>the object could not been removed from the listener list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+94"/>
        <location line="+20"/>
        <location line="+18"/>
        <location line="+18"/>
        <location line="+18"/>
        <location line="+18"/>
        <location line="+18"/>
        <source>not implemented</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
