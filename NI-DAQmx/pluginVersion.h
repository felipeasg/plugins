/* ********************************************************************
    Template for a camera / grabber plugin for the software itom
    
    You can use this template, use it in your plugins, modify it,
    copy it and distribute it without any license restrictions.
*********************************************************************** */

#ifndef PLUGINVERSION_H
#define PLUGINVERSION_H

#define PLUGIN_VERSION_MAJOR 0
#define PLUGIN_VERSION_MINOR 0
#define PLUGIN_VERSION_PATCH 1
#define PLUGIN_VERSION_REVISION 0
#define PLUGIN_VERSION_STRING "0.0.1"
#define PLUGIN_COMPANY        "ITO Uni Stuttgart"
#define PLUGIN_AUTHOR         "M. Hoppe (ITO Uni Stuttgart)"
#define PLUGIN_COPYRIGHT      "(C) 2014, ITO Uni Stuttgart"
#define PLUGIN_NAME           "NI-DAQmx"

//----------------------------------------------------------------------------------------------------------------------------------

#endif // PLUGINVERSION_H
