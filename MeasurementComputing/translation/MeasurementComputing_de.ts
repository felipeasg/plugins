<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de">
<context>
    <name>QObject</name>
    <message>
        <location filename="../MeasurementComputing.cpp" line="+53"/>
        <source>MeasurementComputing-DA-Interface</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>The MeasurementComputing is a itom-PlugIn to give a direct access to a USB digital to analog converter.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>LGPL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>N.A.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MeasurementComputing</name>
    <message>
        <location line="+86"/>
        <source>devices name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>channel parameter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>number of digital devices.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>serial number.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <location line="+5"/>
        <location line="+5"/>
        <source>Pixelsize in x (cols)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-8"/>
        <location line="+5"/>
        <location line="+5"/>
        <source>Pixelsize in y (rows)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Grabdepth of the images</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Total number of analog channels</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Total number of digital channels</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Analog output channel (MeasurementComputing has two analog outputs on channel 13, 14).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>analog output voltage at the output channel, defined by the analog_output_channel parameter.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>A/D range code, if board has a programmable gain. Refer to board specific information for a list of the supported A/D ranges.  (default = UNI5VOLTS)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+191"/>
        <source>Instance with board number already initialized</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+178"/>
        <location line="+31"/>
        <source>DataObject was NULL-Pointer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+13"/>
        <source>DataObject not compatible</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Buffer to small, must be at least 2 bytes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+39"/>
        <source>MeasurementComputing: %1 (value %2)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MeasurementComputingInterface</name>
    <message>
        <location line="-595"/>
        <source>board number</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ito::AddInAlgo</name>
    <message>
        <location filename="../../../../../build/itom/SDK/include/common/addInInterface.h" line="+1023"/>
        <source>uninitialized vector for mandatory parameters!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>uninitialized vector for optional parameters!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>uninitialized vector for output parameters!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
