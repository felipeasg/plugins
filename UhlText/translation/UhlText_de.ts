<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de">
<context>
    <name>DockWidgetUhl</name>
    <message>
        <source>General Information</source>
        <translation type="obsolete">Allgemeine Informationen</translation>
    </message>
    <message>
        <source>Axis:</source>
        <translation type="obsolete">Achsen:</translation>
    </message>
    <message>
        <source>Properties</source>
        <translation type="obsolete">Eigenschaften</translation>
    </message>
    <message>
        <source>enable x</source>
        <translation type="obsolete">x aktiv</translation>
    </message>
    <message>
        <source>enable y</source>
        <translation type="obsolete">y aktiv</translation>
    </message>
    <message>
        <source>enable z</source>
        <translation type="obsolete">z aktiv</translation>
    </message>
    <message>
        <source>enable Joystick</source>
        <translation type="obsolete">Joystick aktiv</translation>
    </message>
    <message>
        <source>Relative Positioning</source>
        <translation type="obsolete">Relative Positionierung</translation>
    </message>
    <message>
        <source>Step Size</source>
        <translation type="obsolete">Schrittweite</translation>
    </message>
    <message>
        <source>Absolut Positioning</source>
        <translation type="obsolete">Absolute Positionierung</translation>
    </message>
    <message>
        <source>Refresh</source>
        <translation type="obsolete">Aktualisieren</translation>
    </message>
    <message>
        <source>x axis</source>
        <translation type="obsolete">x Achse</translation>
    </message>
    <message>
        <source>z axis</source>
        <translation type="obsolete">z Achse</translation>
    </message>
    <message>
        <source>y axis</source>
        <translation type="obsolete">y Achse</translation>
    </message>
    <message>
        <source>Actual</source>
        <translation type="obsolete">Aktuell</translation>
    </message>
    <message>
        <source>Target</source>
        <translation type="obsolete">Ziel</translation>
    </message>
    <message>
        <source>enable</source>
        <translation type="obsolete">aktiv</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>N.A.</source>
        <translation type="obsolete">Nicht verfügbar.</translation>
    </message>
    <message>
        <source>UHL errormessage: </source>
        <translation type="vanished">Uhl Fehlermeldung: </translation>
    </message>
    <message>
        <source>No valid axis name!</source>
        <translation type="vanished">Keine gültige Achsenbezeichnung!</translation>
    </message>
    <message>
        <source>No workable function!</source>
        <translation type="vanished">Keine ausführbare Funktion!</translation>
    </message>
    <message>
        <source>Too many signs in the order string!</source>
        <translation type="vanished">Zu viele Zeichen im Befehlstext!</translation>
    </message>
    <message>
        <source>No valid order!</source>
        <translation type="vanished">Kein gültiger Befehl!</translation>
    </message>
    <message>
        <source>Beyond of valid number range!</source>
        <translation type="vanished">Außerhalb des gültigen Zahlenbereichs!</translation>
    </message>
    <message>
        <source>Wrong number of parameters!</source>
        <translation type="vanished">Falsche Anzahl der Parameter!</translation>
    </message>
    <message>
        <source>Missing &apos;!&apos; or &apos;?&apos;!</source>
        <translation type="vanished">Kein &apos;!&apos; oder &apos;?&apos;!</translation>
    </message>
    <message>
        <source>No trigger mode, this axis is active!</source>
        <translation type="vanished">Kein TVR möglich, da Achse aktiv ist!</translation>
    </message>
    <message>
        <source>No enable or disable of this axis, trigger mode is active!</source>
        <translation type="vanished">Kein Ein- oder Ausschalten der Achsen, da TVR aktiv!</translation>
    </message>
    <message>
        <source>Function does not configure!</source>
        <translation type="vanished">Funktion nicht konfiguriert!</translation>
    </message>
    <message>
        <source>Joystick is on!</source>
        <translation type="vanished">Kein Fahrbefehl möglich, da Joystick auf Handbetrieb!</translation>
    </message>
    <message>
        <source>E-Strike detected!</source>
        <translation type="vanished">Endanschlag betätigt!</translation>
    </message>
    <message>
        <source>Undefined answer from serial port!</source>
        <translation type="vanished">Nicht definierte Antwort der seriellen Schnittstelle!</translation>
    </message>
</context>
<context>
    <name>UhlText</name>
    <message>
        <source>E-Strike detected in Serial Buffer</source>
        <translation type="obsolete">Endanschlag im seriellen Puffer erkannt</translation>
    </message>
    <message>
        <location filename="../UhlText.cpp" line="+332"/>
        <location line="+22"/>
        <source>No signs read after 10 tries</source>
        <translation type="unfinished">Keine Antwort auch nach 10 Versuchen</translation>
    </message>
    <message>
        <location line="-16"/>
        <source>Tried to read float-value but got a string-value</source>
        <translation type="unfinished">Die Antwort konnte ich als Float interpretiert werden</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Tried to read long-value but got a string-value</source>
        <translation type="unfinished">Die Antwort konnte ich als Long interpretiert werden</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>No answer during status request</source>
        <translation type="unfinished">Keine Antwort bei der Statusabfrage</translation>
    </message>
    <message>
        <location line="+26"/>
        <source> in UhlJoystickOn!</source>
        <translation type="unfinished"> in &apos;UhlJoystickOn&apos;!</translation>
    </message>
    <message>
        <location line="+44"/>
        <source>Error while switching joystick off</source>
        <translation type="unfinished">Fehler beim Joystick ausschalten</translation>
    </message>
    <message>
        <location line="+9"/>
        <source> in UhlJoystickOff!</source>
        <translation type="unfinished"> in &apos;UhlJoystickOff&apos;!</translation>
    </message>
    <message>
        <location line="+59"/>
        <source>Undefined answer in serial port buffer, not enough characters</source>
        <translation type="unfinished">Undefinierte Antwort der seriellen Schnittstelle (zu wenig Zeichen)</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>Uhl Setpos: End switch of axis %1 hit</source>
        <translation type="unfinished">Uhl &apos;setPos&apos;: Achse %1 am Endanschlag</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Joystick is activ. Programming error?</source>
        <translation type="unfinished">Joystick ist aktiv. Programmierfehler?</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Unexpected Answer. S or @ expected, but got chr(%1)</source>
        <translation type="unfinished">Unerwartete Antwort. S oder @ erwartet, aber chr(%1) bekommen</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>interrupt occurred</source>
        <translation type="unfinished">Unterbrechung aufgetreten</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>timeout occurred</source>
        <translation type="unfinished">Zeitüberschreitung</translation>
    </message>
    <message>
        <location line="+205"/>
        <source>Accelaration of all axis in mm/s^2</source>
        <translation type="unfinished">Beschleunigung für alle Achsen in mm/s^2</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Speed of all axis in mm/s</source>
        <translation type="unfinished">Geschwindigkeit für alle Achsen in mm/s</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Number of axis at controller</source>
        <translation type="unfinished">Anzahl der Achsen am Controller</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Enabled/disabled Joystick. Default: enabled</source>
        <translation type="unfinished">Joystick ein-/ausschalten. Grundeinstellung: eingeschaltet</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The current com-port ID of this specific device. -1 means undefined</source>
        <translation type="unfinished">Die aktuelle COM-Port-ID ist -1, also undefiniert</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Toggle asynchrone mode of this device</source>
        <translation type="unfinished">Schalter für asynchronen Modus des Geräts</translation>
    </message>
    <message>
        <location line="+46"/>
        <source>Any motor axis is moving. The motor is locked.</source>
        <translation type="unfinished">Ein Achsenmotor verfährt. Der Motor ist geblockt.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>name of requested parameter is empty.</source>
        <translation type="unfinished">Name des abgefragten Parameters ist leer.</translation>
    </message>
    <message>
        <location line="+11"/>
        <location line="+199"/>
        <source>parameter not found in m_params.</source>
        <translation type="unfinished">Parameter wurde in m_params nicht gefunden.</translation>
    </message>
    <message>
        <location line="-166"/>
        <source>name of given parameter is empty.</source>
        <translation type="unfinished">Name des übergebenen Parameters ist leer.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Parameter is read only, input ignored</source>
        <translation type="unfinished">Parameter hat nur Lesezugriff. Die Eingabe wurde ignoriert</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>New value is larger than parameter range, input ignored</source>
        <translation type="unfinished">Der neue Wert ist größer als der zulässige Höchstwert! Eingabe wurde ignoriert</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>New value is smaller than parameter range, input ignored</source>
        <translation type="unfinished">Der neue Wert ist kleiner als der zulässige Mindestwert! Eingabe wurde ignoriert</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Parameter type conflict</source>
        <translation type="unfinished">Typenkonflikt des Parameters</translation>
    </message>
    <message>
        <source>. Expected answer from serial port missed!</source>
        <translation type="obsolete">. Keine Antwort der seriellen Schnittstelle!</translation>
    </message>
    <message>
        <location line="-821"/>
        <source>UHL errormessage: </source>
        <translation type="unfinished">Uhl Fehlermeldung: </translation>
    </message>
    <message>
        <location line="+12"/>
        <source>No valid axis name!</source>
        <translation type="unfinished">Keine gültige Achsenbezeichnung!</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>No executable function!</source>
        <translation type="unfinished">Keine ausführbare Funktion!</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Too many signs in the command string!</source>
        <translation type="unfinished">Zu viele Vorzeichen in der Befehlstext!</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>No valid command!</source>
        <translation type="unfinished">Kein gültiger Befehl!</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Out of valid number range!</source>
        <translation type="unfinished">Außerhalb des zulässigen Speicherbereichs!</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Wrong number of parameters!</source>
        <translation type="unfinished">Falsche Anzahl der Parameter!</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Missing &apos;!&apos; or &apos;?&apos;!</source>
        <translation type="unfinished">Kein &apos;!&apos; oder &apos;?&apos;!</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>No trigger mode possible since this axis is active!</source>
        <translation type="unfinished">Es ist kein Trigger-Modus möglich so lange eine Achse aktiv ist!</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Axis cannot be en- or disabled since trigger mode is active!</source>
        <translation type="unfinished">Solange der Trigger-Modus aktiv ist kann keine Achse aktiviert oder deaktiviert werden!</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Function is not configured!</source>
        <translation type="unfinished">Diese Funktion ist nicht konfiguriert!</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>No move-command allowed since joystick is enabled!</source>
        <translation type="unfinished">Solange der Joystick aktiv ist, ist kein Verfahrbefehl erlaubt!</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>End-switch triggered!</source>
        <translation type="unfinished">Endanschlag!</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Undefined answer from serial port!</source>
        <translation type="unfinished">Nicht definierte Antwort der seriellen Schnittstelle!</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>&apos;S&apos; detected in answer from device (end-switch reached by any axis???)</source>
        <translation type="unfinished">Es wurde in der Antwort des Geräts ein &apos;S&apos; gefunden. (Hat eine Achse den Endanschlag erreicht?)</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&apos;J&apos; detected in answer from device (joystick mode of any axis)</source>
        <translation type="unfinished">Es wurde in der Antwort des Geräts ein &apos;J&apos; gefunden. (Eine Achse befindet sich im Joystick-Modus.)</translation>
    </message>
    <message>
        <location line="+803"/>
        <source>Too small acceleration</source>
        <translation type="unfinished">Zu geringe Beschleunigung</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Too high acceleration</source>
        <translation type="unfinished">Zu große Beschleunigung</translation>
    </message>
    <message>
        <location line="+64"/>
        <source> in Uhl::SetParam for %s</source>
        <translation type="unfinished"> in &apos;Uhl::SetParam&apos; während %s</translation>
    </message>
    <message>
        <location line="+182"/>
        <source>Doesn&apos;t fit to interface DataIO!</source>
        <translation type="unfinished">Die Schnittstelle ist keine serielle Schnittstelle!</translation>
    </message>
    <message>
        <location line="+5"/>
        <location line="+44"/>
        <location line="+52"/>
        <location line="+267"/>
        <source> in UHL::INIT</source>
        <translation type="unfinished"> in &apos;UHL::INIT&apos;</translation>
    </message>
    <message>
        <location line="-354"/>
        <source> during port number read out</source>
        <translation type="unfinished"> beim Auslesen der Portnummer</translation>
    </message>
    <message>
        <location line="+25"/>
        <source> in UHL::INIT for sending status</source>
        <translation type="unfinished"> in &apos;UHL::INIT&apos; beim Senden des Statuses</translation>
    </message>
    <message>
        <location line="+42"/>
        <source> in UHL::INIT for saving old position</source>
        <translation type="unfinished"> in &apos;UHL::INIT&apos; beim Speichern der alte Position</translation>
    </message>
    <message>
        <location line="+12"/>
        <source> in UHL::INIT for sending reset</source>
        <translation type="unfinished"> in &apos;UHL::INIT&apos; beim Senden von Reset</translation>
    </message>
    <message>
        <location line="+18"/>
        <source> in UHL::INIT sending U7mb</source>
        <translation type="unfinished"> in &apos;UHL::INIT&apos; beim Senden von &apos;U7mb&apos;</translation>
    </message>
    <message>
        <location line="+16"/>
        <source> in UHL::INIT sending ?det</source>
        <translation type="unfinished"> in &apos;UHL::INIT&apos; beim Senden von &apos;?det&apos;</translation>
    </message>
    <message>
        <location line="+51"/>
        <source>For this UHL table use the UhlRegister driver!</source>
        <translation type="unfinished">Für diesen Uhltisch bitte den UhlRegister-Treiber benutzen!</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Stupid number of axis specified from uhltable</source>
        <translation type="unfinished">Dem Uhltisch wurde eine ungültige Achsennummer übergeben</translation>
    </message>
    <message>
        <location line="+26"/>
        <source> in UHL::INIT sending !!joydir</source>
        <translation type="unfinished"> in &apos;UHL::INIT&apos; beim Senden von &apos;!!joydir&apos;</translation>
    </message>
    <message>
        <location line="+17"/>
        <source> in UHL::INIT sending !dim</source>
        <translation type="unfinished"> in &apos;UHL::INIT&apos; beim Senden von &apos;!dim&apos;</translation>
    </message>
    <message>
        <location line="+9"/>
        <location line="+25"/>
        <source> in UHL::INIT sending ?err</source>
        <translation type="unfinished"> in &apos;UHL::INIT&apos; beim Senden von &apos;?err&apos;</translation>
    </message>
    <message>
        <location line="-17"/>
        <source> in UHL::INIT reading ?err</source>
        <translation type="unfinished"> in &apos;UHL::INIT&apos; beim Lesen von &apos;?err&apos;</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>uhlTextController::Init uhlcommand @ dim crashed</source>
        <translation type="unfinished">&apos;uhlTextController::Init&apos; Uhl-Befehl @ dim führte zum Absturz</translation>
    </message>
    <message>
        <location line="+39"/>
        <source> in UHL::INIT sending ?pitch</source>
        <translation type="unfinished"> in &apos;UHL::INIT&apos; beim Senden von &apos;?pitch&apos;</translation>
    </message>
    <message>
        <location line="+8"/>
        <source> in UHL::INIT reading ?pitch</source>
        <translation type="unfinished"> in &apos;UHL::INIT&apos; beim Lesen von &apos;?pitch&apos;</translation>
    </message>
    <message>
        <location line="+9"/>
        <source> in UHL::INIT sending ?vel</source>
        <translation type="unfinished"> in &apos;UHL::INIT&apos; beim Senden von &apos;?vel&apos;</translation>
    </message>
    <message>
        <location line="+8"/>
        <source> in UHL::INIT reading ?vel</source>
        <translation type="unfinished"> in &apos;UHL::INIT&apos; beim Lesen von &apos;?vel&apos;</translation>
    </message>
    <message>
        <location line="+24"/>
        <source> in UHL::INIT sending ?accel</source>
        <translation type="unfinished"> in &apos;UHL::INIT&apos; beim Senden von &apos;?accel&apos;</translation>
    </message>
    <message>
        <location line="+8"/>
        <source> in UHL::INIT reading ?accel</source>
        <translation type="unfinished"> in &apos;UHL::INIT&apos; beim Lesen von &apos;?accel&apos;</translation>
    </message>
    <message>
        <location line="+41"/>
        <location line="+7"/>
        <source> in UHL::INIT for writing old position</source>
        <translation type="unfinished"> in &apos;UHL::INIT&apos; beim Schreiben der alten Position</translation>
    </message>
    <message>
        <location line="+86"/>
        <source>Any motor axis is already moving</source>
        <translation type="unfinished">Ein Achsenmotor verfährt bereits</translation>
    </message>
    <message>
        <location line="+51"/>
        <source> in UHL::CALIB</source>
        <translation type="unfinished"> in &apos;UHL::CALIB&apos;</translation>
    </message>
    <message>
        <location line="+241"/>
        <location line="+77"/>
        <source>Axis not exist</source>
        <translation type="unfinished">Achse existiert nicht</translation>
    </message>
    <message>
        <location line="+144"/>
        <source>Axis was specified twice</source>
        <translation type="unfinished">Eine Achse wurde mehrfach spezifiziert</translation>
    </message>
</context>
<context>
    <name>UhlTextInterface</name>
    <message>
        <location line="-2244"/>
        <source>DLL for 2-4 axis Uhl / Lang LStep-Controller</source>
        <translation type="unfinished">DLL für 2-4 achsige Uhltische / LStep-Kontroller</translation>
    </message>
    <message>
        <source>The UhlText is a iTom-PlugIn, which can be used to control the 2-4 axis
stepper motor devices from Uhl (F9S-x) and Lang LSTEP 2x
It is initialized by actuator(&quot;UhlText&quot;, SerialIO, ...).
WARNING: There are different controller versions with different
command languages. This DLL is for ??? devices.
WARNING: The calibration direction of the stages differes according to motor / controller.
Check calibration direction before usage.</source>
        <translation type="obsolete">UhlText ist ein itom-Plugin, welches zur Steuerung für 2-4 achsige Schrittmotorgeräten von Uhl (F9S-x) und Lang LSTEP 2x verwendet werden kann.
Die Initialisierung erfolg über &apos;actuator(&quot;UhlText&quot;, SerialIO, ...)&apos;.
WARNUNG: Es gibt unterschiedliche Controller-Versionen mit unterschiedlichen Befehlen. Diese DLL ist für ??? Geräte.
WARNUNG: Die Kalibrierrichtung der Achsen unterscheiden sich je nach Motor/Controller. Vor der Benutzung die Fahrrichtung der Kalibrierung überprüfen.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>The UhlText is a plugin, which can be used to control the 2-4 axis
stepper motor devices from Uhl (F9S-x) and Lang LSTEP 2x
It is initialized by actuator(&quot;UhlText&quot;, SerialIO, ...).

WARNING: There are different controller versions with different
command languages. This DLL is for devices that are controlled by ASCII commands via the RS232 port.
WARNING: The calibration direction of the stages differs according to motor / controller.
Check calibration direction before usage. 

This plugin was published with the kind permission of company Walter Uhl, technische Mikroskopie GmbH &amp; Co. KG.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+14"/>
        <source>LGPL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>N.A.</source>
        <translation type="unfinished">Nicht verfügbar.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>An initialized SerialIO</source>
        <translation type="unfinished">Eine initialisierte SerialIO-Instanz</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>1 -&gt; calibration during initilization</source>
        <translation type="unfinished">1 -&gt; Kalibrierung während der Initialisierung</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Invert axis direction for x</source>
        <translation type="unfinished">Dreht die Fahrtrichtung der x-Achse um</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Invert axis direction for y</source>
        <translation type="unfinished">Dreht die Fahrtrichtung der y-Achse um</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Invert axis direction for z</source>
        <translation type="unfinished">Dreht die Fahrtrichtung der z-Achse um</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Invert axis direction for a</source>
        <translation type="unfinished">Dreht die Fahrtrichtung der a-Achse um</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Enabled/disabled Joystick. Default: enabled</source>
        <translation type="unfinished">Joystick ein-/ausschalten. Grundeinstellung: eingeschaltet</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>New baud rate for uhl table</source>
        <translation type="unfinished">Neue Baud-Rate für den Uhltisch</translation>
    </message>
</context>
<context>
    <name>dialogUhl</name>
    <message>
        <location filename="../../UhlRegister/dialogUhl.cpp" line="+31"/>
        <source>Configuration Dialog</source>
        <translation>Konfigurationsdialg</translation>
    </message>
    <message>
        <location line="+138"/>
        <source>timeout while setting parameters of plugin.</source>
        <translation>Zeitüberschreitung während Parameter setzen.</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>plugin instance not defined.</source>
        <translation>Plugin-Instanz nicht definiert.</translation>
    </message>
    <message>
        <source>Configuration Uhl Actuator</source>
        <translation type="obsolete">Uhltisch-Konfiguration</translation>
    </message>
    <message>
        <source>Speed</source>
        <translation type="obsolete">Geschwindigkeit</translation>
    </message>
    <message>
        <source>Accerlaration</source>
        <translation type="obsolete">Beschleunigung</translation>
    </message>
    <message>
        <source>Speed of Uhl Actuator</source>
        <translation type="obsolete">Geschwindigkeit des Uhltischs</translation>
    </message>
    <message>
        <source>Accerlaration of Uhl Actuator</source>
        <translation type="obsolete">Beschleunigung des Uhltischs</translation>
    </message>
    <message>
        <source>Set current position as null position</source>
        <translation type="obsolete">Setzt aktuelle Position als Null-Position</translation>
    </message>
    <message>
        <source>Origin Y</source>
        <translation type="obsolete">Null-Pos. Y</translation>
    </message>
    <message>
        <source>Origin Z</source>
        <translation type="obsolete">Null-Pos. Z</translation>
    </message>
    <message>
        <source>Change the direction of X-axis</source>
        <translation type="obsolete">Dreht die Fahrtrichtung der X-Achse um</translation>
    </message>
    <message>
        <source>Invert X</source>
        <translation type="obsolete">Invert. X</translation>
    </message>
    <message>
        <source>Origin X</source>
        <translation type="obsolete">Null-Pos. X</translation>
    </message>
    <message>
        <source>Enable or Disable the X-axis</source>
        <translation type="obsolete">Schaltet die X-Achse ein oder aus</translation>
    </message>
    <message>
        <source>Enable X</source>
        <translation type="obsolete">X Aktiv</translation>
    </message>
    <message>
        <source>Enable or Disable the Y-axis</source>
        <translation type="obsolete">Schaltet die Y-Achse ein oder aus</translation>
    </message>
    <message>
        <source>Enable Y</source>
        <translation type="obsolete">Y Aktiv</translation>
    </message>
    <message>
        <source>Change the direction of Y-axis</source>
        <translation type="obsolete">Dreht die Fahrtrichtung derYx-Achse um</translation>
    </message>
    <message>
        <source>Invert Y</source>
        <translation type="obsolete">Invert. Y</translation>
    </message>
    <message>
        <source>Change the direction of Z-axis</source>
        <translation type="obsolete">Dreht die Fahrtrichtung der Z-Achse um</translation>
    </message>
    <message>
        <source>Invert Z</source>
        <translation type="obsolete">Invert. Z</translation>
    </message>
    <message>
        <source>Enable or Disable the Z-axis</source>
        <translation type="obsolete">Schaltet die Z-Achse ein oder aus</translation>
    </message>
    <message>
        <source>Enable Z</source>
        <translation type="obsolete">Z Aktiv</translation>
    </message>
    <message>
        <source>Moves all axes to their null position</source>
        <translation type="obsolete">Fährt alle Achsen auf deren Null-Position</translation>
    </message>
    <message>
        <source>Calibrate</source>
        <translation type="obsolete">Kalibrieren</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Abbrechen</translation>
    </message>
    <message>
        <source>Properties</source>
        <translation type="obsolete">Eigenschaften</translation>
    </message>
    <message>
        <source>Axis</source>
        <translation type="obsolete">Achsen</translation>
    </message>
</context>
<context>
    <name>ito::AddInActuator</name>
    <message>
        <source>Constructor must be overwritten</source>
        <translation type="obsolete">Konstruktor muss überschrieben werden</translation>
    </message>
    <message>
        <source>Destructor must be overwritten</source>
        <translation type="obsolete">Destruktor muss überschrieben werden</translation>
    </message>
</context>
<context>
    <name>ito::AddInAlgo</name>
    <message>
        <source>Constructor must be overwritten</source>
        <translation type="obsolete">Konstruktor muss überschrieben werden</translation>
    </message>
    <message>
        <source>uninitialized vector for mandatory parameters!</source>
        <translation type="obsolete">Nicht initialisierter Vektor für Pflichtparameter!</translation>
    </message>
    <message>
        <source>uninitialized vector for optional parameters!</source>
        <translation type="obsolete">Nicht initialisierter Vektor für optionale Parameter!</translation>
    </message>
    <message>
        <source>uninitialized vector for output parameters!</source>
        <translation type="obsolete">Nicht initialisierter Vektor fürAusgabeparameter!</translation>
    </message>
</context>
<context>
    <name>ito::AddInBase</name>
    <message>
        <source>function execution unused in this plugin</source>
        <translation type="obsolete">Die Funktion &apos;execution&apos; wurde in diesem Plugin nicht bentzt</translation>
    </message>
    <message>
        <source>Your plugin is supposed to have a configuration dialog, but you did not implement the showConfDialog-method</source>
        <translation type="obsolete">Das Plugin scheint einen Konfigurationsdialog zu besitzen, doch die Methode showConfDialog wurde nicht implementiert</translation>
    </message>
</context>
<context>
    <name>ito::AddInDataIO</name>
    <message>
        <source>Constructor must be overwritten</source>
        <translation type="obsolete">Konstruktor muss überschrieben werden</translation>
    </message>
    <message>
        <source>Destructor must be overwritten</source>
        <translation type="obsolete">Destruktor muss überschrieben werden</translation>
    </message>
    <message>
        <source>timer could not be set</source>
        <translation type="obsolete">Timer konnte nicht gesetzt werden</translation>
    </message>
    <message>
        <source>not implemented</source>
        <translation type="obsolete">Nicht implementiert</translation>
    </message>
</context>
</TS>
