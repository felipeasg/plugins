<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de">
<context>
    <name>DialogDummyGrabber</name>
    <message>
        <location filename="../dialogDummyGrabber.cpp" line="+49"/>
        <source>Configuration Dialog</source>
        <translation type="unfinished">Konfigurationsdialog</translation>
    </message>
</context>
<context>
    <name>DockWidgetDummyGrabber</name>
    <message>
        <location filename="../dockWidgetDummyGrabber.ui" line="+35"/>
        <source>General Information</source>
        <translation>Allgemeine Informationen</translation>
    </message>
    <message>
        <location line="+47"/>
        <source>Image Dimensions</source>
        <translation>Bild</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Width:</source>
        <translation>Breite:</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Height:</source>
        <translation>Höhe:</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Bits per Pixel:</source>
        <translation>Bits pro Pixel:</translation>
    </message>
    <message>
        <location line="+48"/>
        <source> bits</source>
        <translation> Bits</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Integration</source>
        <translation>Belichtung</translation>
    </message>
    <message>
        <location line="+38"/>
        <location line="+33"/>
        <source> %</source>
        <translation></translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Integrationtime</source>
        <translation>Belichtungszeit</translation>
    </message>
    <message>
        <location line="+10"/>
        <source> ms</source>
        <translation></translation>
    </message>
    <message>
        <location line="-255"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location line="+39"/>
        <source>[ID]</source>
        <translation></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>ID:</source>
        <translation></translation>
    </message>
    <message>
        <location line="+58"/>
        <location line="+19"/>
        <source> px</source>
        <translation></translation>
    </message>
    <message>
        <location line="+56"/>
        <source>Offset</source>
        <translation></translation>
    </message>
    <message>
        <location line="+33"/>
        <source>Gain</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>DummyGrabber</name>
    <message>
        <location filename="../DummyGrabber.cpp" line="+230"/>
        <source>Minimum time between the start of two consecutive acquisitions [s], default: 0.0.</source>
        <translation type="unfinished">Mindestzeit zwischen zwei fortlaufenden Bildanforderungen [s], standard: 0,0.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Virtual gain</source>
        <translation type="unfinished">Virtuelles Gain</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Virtual offset</source>
        <translation type="unfinished">Virtuelles Offset</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Binning of different pixel, binning = x-factor * 100 + y-factor</source>
        <translation type="unfinished">Binning unterschiedlicher Pixel, binning = x-factor * 100 + y-factor</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>ROI (x,y,width,height) [this replaces the values x0,x1,y0,y1]</source>
        <translation type="unfinished">ROI (x, y, Breite, Höhe) [ersetzt die Werte x0, x1, y0, y1]</translation>
    </message>
    <message>
        <location line="+375"/>
        <source>stopDevice of DummyGrabber can not be executed, since camera has not been started.</source>
        <translation type="unfinished">&apos;stopDevice&apos; des DummyGrabbers kann nicht ausgeführt werden, da die Kamera nicht gestartet wurde.</translation>
    </message>
    <message>
        <location line="+46"/>
        <source>wrong bit depth</source>
        <translation type="unfinished">Falsche Bittiefe</translation>
    </message>
    <message>
        <location line="+221"/>
        <source>image could not be obtained since no image has been acquired.</source>
        <translation type="unfinished">Es konnte kein Bild abgeholt werden, da keines angefordert wurde.</translation>
    </message>
    <message>
        <location line="-648"/>
        <source>size in x (cols) [px]</source>
        <translation type="unfinished">Größe in x (Spalten) [px]</translation>
    </message>
    <message>
        <location line="-10"/>
        <source>Minimum integration time for an acquisition [s], default: 0.0 (as fast as possible).</source>
        <translation type="unfinished">Mindest-&apos;integration time&apos; für eine Bildanforderungen [s], standard: 0,0 (so schnell wie möglich).</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>size in y (rows) [px]</source>
        <translation type="unfinished">Größe in y (Zeilen) [px]</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>bitdepth of images</source>
        <translation type="unfinished">Bittiefe des Images</translation>
    </message>
    <message>
        <location line="+420"/>
        <source>Acquire of DummyGrabber can not be executed, since camera has not been started.</source>
        <translation type="unfinished">&apos;acquire&apos; kann vom DummyGrabber nicht ausgeführt werden, da die Kamera nicht gestartet wurde.</translation>
    </message>
    <message>
        <location line="+144"/>
        <source>data object of getVal is NULL or cast failed</source>
        <translation type="unfinished">Das Datenobjekt von &apos;getVal&apos; ist NULL oder enthält ein falsches Bildformat</translation>
    </message>
    <message>
        <location line="+40"/>
        <source>Empty object handle retrieved from caller</source>
        <translation type="unfinished">Es wurde ein leeres Objekt übergeben</translation>
    </message>
</context>
<context>
    <name>DummyGrabberInterface</name>
    <message>
        <location line="-699"/>
        <source>N.A.</source>
        <translation>Nicht verfügbar.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Width of virtual sensor chip</source>
        <translation type="unfinished">Breite des virtuellen Sensors</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Height of virtual sensor chip, please set this value to 1 (line camera) or a value dividable by 4 for a 2D camera.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Height of virtual sensor chip</source>
        <translation type="obsolete">Höhe des virtuellen Sensors</translation>
    </message>
    <message>
        <source>Maximum x size of image</source>
        <translation type="obsolete">Maximale Bildbreite</translation>
    </message>
    <message>
        <source>Maximum y size of image</source>
        <translation type="obsolete">Maximale Bildhöhe</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Bits per Pixel, usually 8-16bit grayvalues</source>
        <translation type="unfinished">Bits pro Pixel, normalerweise 8-16 Bit Grauwerte</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location line="-28"/>
        <source>A virtual white noise grabber</source>
        <translation type="unfinished">Ein virtueller Weißrausch-Grabber</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>The DummyGrabber is a virtual camera which emulates a camera with white noise. 

The camera is initialized with a maximum width and height of the simulated camera chip (both need to be a multiple of 4). The noise is always scaled in the range between 0 and the current bitdepth (bpp - bit per pixel). The real size of the camera image is controlled using the parameter &apos;roi&apos; if the sizes stay within the limits given by the size of the camera chip.

You can initialize this camera either as a 2D sensor with a width and height &gt;= 4 or as line camera whose height is equal to 1. 

This plugin can also be used as template for other grabber.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Licensed under LPGL.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>dialogDummyGrabber</name>
    <message>
        <source>Buffer and Binning</source>
        <translation type="obsolete">Puffer und Binning</translation>
    </message>
    <message>
        <source>BitPerPix</source>
        <translation type="obsolete">BitProPix</translation>
    </message>
    <message>
        <source>Apply</source>
        <translation type="obsolete">Übernehmen</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Abbrechen</translation>
    </message>
    <message>
        <location filename="../dialogDummyGrabber.ui" line="+157"/>
        <source>Integration</source>
        <translation type="unfinished">Belichtung</translation>
    </message>
    <message>
        <location line="+100"/>
        <source>Integrationtime</source>
        <translation type="unfinished">Belichtungszeit</translation>
    </message>
    <message>
        <location line="+13"/>
        <location line="+35"/>
        <source> ms</source>
        <translation></translation>
    </message>
    <message>
        <location line="-13"/>
        <source>Frametime</source>
        <translation type="unfinished">Zeitrahmen</translation>
    </message>
    <message>
        <location line="+41"/>
        <source>Size</source>
        <translation type="unfinished">Größe</translation>
    </message>
    <message>
        <source>XSize</source>
        <translation type="obsolete">X-Größe</translation>
    </message>
    <message>
        <source>YSize</source>
        <translation type="obsolete">Y-Größe</translation>
    </message>
    <message>
        <source>Configuration Dialog</source>
        <translation type="vanished">Konfigurationsdialog</translation>
    </message>
    <message>
        <source>ROI (Binning changed, press apply or save)</source>
        <translation type="vanished">RIO (Binning wurde geändert, auf Übernehmen oder Speichern klicken)</translation>
    </message>
    <message>
        <location line="+99"/>
        <source>x-size</source>
        <translation type="unfinished">X-Größe</translation>
    </message>
    <message>
        <location line="+19"/>
        <location line="+29"/>
        <source> px</source>
        <translation></translation>
    </message>
    <message>
        <location line="-16"/>
        <source>y-size</source>
        <translation type="unfinished">Y-Größe</translation>
    </message>
    <message>
        <source>Region of Interest (ROI)</source>
        <translation type="vanished">Bildausschnitt (ROI)</translation>
    </message>
    <message>
        <source>reset to full size</source>
        <translation type="obsolete">Vollbild</translation>
    </message>
    <message>
        <location line="-438"/>
        <source>Dialog</source>
        <translation></translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Buffer</source>
        <translation type="unfinished">Puffer</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Binning X</source>
        <translation></translation>
    </message>
    <message>
        <location line="+8"/>
        <location line="+32"/>
        <source>1</source>
        <translation></translation>
    </message>
    <message>
        <location line="-27"/>
        <location line="+32"/>
        <source>2</source>
        <translation></translation>
    </message>
    <message>
        <location line="-27"/>
        <location line="+32"/>
        <source>4</source>
        <translation></translation>
    </message>
    <message>
        <location line="-18"/>
        <source>Binning Y</source>
        <translation></translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Bits per Pixel</source>
        <translation type="unfinished">Bits pro Pixel</translation>
    </message>
    <message>
        <location line="+222"/>
        <source>x0</source>
        <translation></translation>
    </message>
    <message>
        <location line="+35"/>
        <source>x1</source>
        <translation></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>y0</source>
        <translation></translation>
    </message>
    <message>
        <location line="+38"/>
        <source>y1</source>
        <translation></translation>
    </message>
    <message>
        <location line="+85"/>
        <source>full ROI</source>
        <translation type="unfinished">Komplettes RIO</translation>
    </message>
    <message>
        <location line="-343"/>
        <source>Offset</source>
        <translation></translation>
    </message>
    <message>
        <location line="+26"/>
        <location line="+55"/>
        <source>%</source>
        <translation></translation>
    </message>
    <message>
        <location line="-26"/>
        <source>Gain</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ito::AddInAlgo</name>
    <message>
        <source>uninitialized vector for mandatory parameters!</source>
        <translation type="obsolete">Nicht initialisierter Vektor für Pflichtparameter!</translation>
    </message>
    <message>
        <source>uninitialized vector for optional parameters!</source>
        <translation type="obsolete">Nicht initialisierter Vektor für optionale Parameter!</translation>
    </message>
    <message>
        <source>uninitialized vector for output parameters!</source>
        <translation type="obsolete">Nicht initialisierter Vektor fürAusgabeparameter!</translation>
    </message>
</context>
</TS>
