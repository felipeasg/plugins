<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de">
<context>
    <name>BasicFilters</name>
    <message>
        <location filename="../BasicFilters.cpp" line="+204"/>
        <source>Set each pixel to the lowest value within the kernel (x ,y) using the generic mcpp filter engine</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Set each pixel to the highest value within the kernel (x ,y) using the generic mcpp filter engine</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Performs a median filter with kernelsize (x ,y) using the generic mcpp filter engine</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Performs a low-pass filter with kernelsize (x ,y) using the generic mcpp filter engine</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Performs a gaussian blur filter according to sigma and epsilon using the generic mcpp filter engine. The kernelsize (x ,y) will be estimated.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Performs a gaussian blur filter with kernelsize (x ,y) and according to sigmaX and sigmaY using the generic mcpp filter engine</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BasicSpecialFilters.cpp" line="+3335"/>
        <location filename="../BasicFilters.cpp" line="+37"/>
        <source>Input image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BasicFilters.cpp" line="+2"/>
        <source>Output image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BasicGenericFilters.cpp" line="+449"/>
        <location line="+1953"/>
        <location line="+238"/>
        <source>n-dim DataObject</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-2189"/>
        <location line="+1953"/>
        <location line="+238"/>
        <source>n-dim DataObject of type sourceImage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-2189"/>
        <location line="+1953"/>
        <location line="+463"/>
        <source>Odd kernelsize in x</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-2414"/>
        <location line="+1953"/>
        <location line="+463"/>
        <source>Odd kernelsize in y</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-2414"/>
        <location line="+1957"/>
        <location line="+238"/>
        <source>if 0 NaN values in input image will be copied to output image (default)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-1737"/>
        <location line="+572"/>
        <location line="+350"/>
        <location line="+603"/>
        <location line="+238"/>
        <location line="+380"/>
        <source>Source object not defined</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-2139"/>
        <location line="+572"/>
        <location line="+350"/>
        <location line="+603"/>
        <location line="+238"/>
        <source>Ito data object is empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-1759"/>
        <location line="+572"/>
        <location line="+350"/>
        <location line="+603"/>
        <location line="+238"/>
        <location line="+380"/>
        <source>Destination object not defined</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-2110"/>
        <location line="+572"/>
        <location line="+350"/>
        <location line="+602"/>
        <location line="+614"/>
        <source>Error: kernel in x must be odd</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-2133"/>
        <location line="+572"/>
        <location line="+350"/>
        <location line="+602"/>
        <location line="+614"/>
        <source>Error: kernel in y must be odd</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-1900"/>
        <source>high value filter with kernel %1 x %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Low value filter with kernel %1 x %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <location line="+447"/>
        <location line="+350"/>
        <location line="+608"/>
        <location line="+225"/>
        <source> and removed NaN-values</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-1188"/>
        <source>median filter with kernel %1 x %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+350"/>
        <source>lowpass-filter (mean) with kernel %1 x %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+408"/>
        <location line="+2"/>
        <location line="+232"/>
        <location line="+4"/>
        <source>Standard deviation in x direction</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-38"/>
        <location line="+225"/>
        <source>gaussian-filter with sigma %1 x %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-189"/>
        <source>Stop condition in x-direction, kernel values less than epsilon are ignored</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Stop condition in y-direction, kernel values less than epsilon are ignored</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+215"/>
        <source>source dataObject of any real data type.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>destination dataObject (can be the same than source)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Delta value for comparison</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>value set to clipped values</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>if 1 (default), a spike value is replaced by the parameter &apos;newValue&apos;, else it is replaced by the value from the filtered object.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+185"/>
        <source>source object must not be empty.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+47"/>
        <location line="+8"/>
        <location line="+11"/>
        <location line="+11"/>
        <location line="+11"/>
        <location line="+39"/>
        <source> while running spike removal by median filter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-8"/>
        <source> while running spike removal by mean filter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Error: filter type not implemented for generic spike filter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+94"/>
        <source>spike removal filter via median filter with kernel %1 x %2 for delta = %3 and newValue %4 </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BasicSpecialFilters.cpp" line="-3141"/>
        <location line="+139"/>
        <location line="+72"/>
        <location line="+169"/>
        <location line="+1973"/>
        <source>Error: source image ptr empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-2348"/>
        <location line="+139"/>
        <location line="+77"/>
        <location line="+2137"/>
        <location line="+403"/>
        <source>Error: dest image ptr empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-2751"/>
        <source>Error: Input image must be 3D</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Error: one dimension of input image must be equal to 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+87"/>
        <location line="+85"/>
        <location line="+717"/>
        <source>Unknown type or type not implemented</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-794"/>
        <source>Flattened object from 3d to 2d</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+39"/>
        <source>Error: Input image must be 2D</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+44"/>
        <source>Swap byte order</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+23"/>
        <location line="+169"/>
        <source>Error: replace image ptr empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-159"/>
        <source>source and replace image must have the same type and size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Error: this filter is only usable for float or double matrices</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+89"/>
        <source>replace NaN and infinity values</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+34"/>
        <source>Input image with 3 or 4 uint8 planes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Switch between RGBA = 0, BGRA = 1, ARGB = 2, ABGR = 3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+206"/>
        <source>Merged from multiplane color object</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-230"/>
        <source>Input object of type float32 or float64 whose non-finite values will be replaced with the values in replaceImg.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>replacement data object of same type and shape than srcImg.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Output object of same type and shape than srcImg (inplace possible).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>number of values that have been replaced.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Output image with of type rgba32</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+43"/>
        <source>SrcImg must be three dimensional, of type uint8 and contain three or four planes.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+179"/>
        <source>3D input object of dimension [ZxMxN] with Z different planes.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Output image of same type than input object and dimension [1xMxN] or [2xMxN] depending on parameter &apos;calcStd&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>If 1, Inf or NaN values will not be taken into account when calculating the mean value (default, only important for floating point data types).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>If 1, the standard deviation is also calculated and put into the second plane of &apos;destImg&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+206"/>
        <source>Error: sourceImageStack is Null-Pointer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+17"/>
        <source>destinationPlane is a uninitialized dataObject!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+102"/>
        <source>Calculated mean value in z-Direction from 3D-Object</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+25"/>
        <source>2D image or single plane n-D object</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Slice with output data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>x0-coordinate for slice</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>y0-coordinate for slice</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>x1-coordinate for slice</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>y1-coordinate for slice</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>0: Bresenham or Nearest, 1: weighted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+26"/>
        <location line="+5"/>
        <source>Error: sourceImage is Null-Pointer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Error: sourceImage must have at least 2 dimensions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Error: sourceImage must not have more than 1 plane</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+234"/>
        <source>slice has not defined size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+158"/>
        <source>datatype not supported</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>matrix step vector for matrix is empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Cut 1D slice out of 2D-data from [ %2, %3] to [ %4, %5]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+125"/>
        <location line="+377"/>
        <source>input image [real typed data object]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-375"/>
        <location line="+379"/>
        <source>destination image (inplace possible)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-377"/>
        <location line="+379"/>
        <source>lowest value in range</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-377"/>
        <location line="+379"/>
        <source>highest value in range</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+410"/>
        <source>2D or multidimensional source data object ((u)int8, (u)int16, (u)int32, float32, float64 or rgba32)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>histogram data object (will be int32, [higher-dimensions x 1 x bins] where higher-dimensions corresponds to the dimensions higher than x and y of the source object. A source object of type rgba32 will lead to [higher-dimensions x 4 x bins] where the 4-sized dimension is the histogram if the [r,g,b,gray] value channels. If the given object already fits to the type and size requirements, it is used without allocating a new object.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Number of bins ((u)int16, (u)int32, float32 and float64 only, default: 0 leads to 1024 bins), for (u)int8 and rgba32 the number of bins is given by the total number of values represented by the data type.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Defines how to determine the interval of the histogram: -1 (default) use the limits of (u)int8 and rgba32 and auto-calculate the min/max values for floating point data types, 0: use the values given by interval, 1: automatically calculate the min/max values for all data types.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Interval of the histogram (depending on parameter &apos;autoInterval&apos;). The first value is included in the first bin, the last value is included in the last bin.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+102"/>
        <source>Histogram can only be calculated for (u)int8, (u)int16, (u)int32, float32, float64 or rgba32</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+127"/>
        <source>Calculated histogramm between %1 : %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+586"/>
        <source>Output image (1xN) of the same type than the input image, where N corresponds to different radiuses.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>step size of the radius for the discretization</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>x-coordinate of radial center</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>y-coordinate of radial center</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-1616"/>
        <location line="+379"/>
        <source>value set to clipped values (default: 0.0)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-377"/>
        <location line="+379"/>
        <source>0: clip values outside of given range (default), 1: clip inside</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-355"/>
        <location line="+380"/>
        <source>Error: source image empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-375"/>
        <location line="+385"/>
        <source>Error: dest image empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-380"/>
        <location line="+385"/>
        <location line="+409"/>
        <source>Error: source is not a matrix or image stack</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-789"/>
        <location line="+394"/>
        <source>Error: minValue must be smaller than maxValue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-244"/>
        <location line="+335"/>
        <location line="+104"/>
        <source>unknown type or type not implemented (e.g. complex)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-431"/>
        <location line="+439"/>
        <source>Clipped values outside %1 : %2 to %3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-435"/>
        <location line="+439"/>
        <source>Clipped values inside %1 : %2 to %3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-272"/>
        <source>input image [real typed data object] for comparision</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+41"/>
        <source>Error: comparison image empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+634"/>
        <source>Preallocated dataObject to be filled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Geometric primitiv according to definition</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Switch between fill inside, outside or both</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Edge-Flag, currently not used</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>New value for pixels inside the geometric element</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>New value for pixels outside the geometric element</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+168"/>
        <source>Error: geometricElement ptr empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Error: source is not a 2d matrix</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Error: geometricElement is not a 2d matrix</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Error: geometricElement must be either float32 or float64</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+38"/>
        <location line="+75"/>
        <location line="+98"/>
        <location line="+22"/>
        <location line="+22"/>
        <location line="+22"/>
        <location line="+22"/>
        <location line="+22"/>
        <location line="+22"/>
        <source>Error: geometric primitiv not supported for filling</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-191"/>
        <source>Error: geometricElement must be either marker-style (8x1) or primitiv-style (1x11)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Error: radii of geometricElement must not be zero</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Error: coordinates of geometricElement must be finite</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+160"/>
        <source>Error: destination object type is not suppirted for filling</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../BasicFilters.cpp" line="-202"/>
        <source>ITO developed filter-functions for data objects</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>This plugin provides several basic filter calculations for itom::dataObject. These are for instance: 

* merging of planes
* swap byte order of objects 
* resample slices from dataObjects 
* mean value filter along axis 

This plugin does not have any unusual dependencies.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+14"/>
        <source>LGPL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>N.A.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+50"/>
        <source>replaces infinite and/or nan-values by values of second matrix. 

</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Flattens a z-Stack of Mx1xN or MxNx1 matrixes to NxM or MxN. 

</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Swap byte order for input image. 

</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Merge 3 or 4 color planes to a single tRGBA32 or tInt32-plane. 

If second object is tInt32 and of right size in x and y, the stack object will be convertet to tInt32. In all other cases the object will be tRGBA32 

</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Calculate mean value (and optional standard deviation) of a 3D data object in z-direction. 

</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Interpolate 1D-slice from along the defined line from a 2D-Object. 

</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>clip values outside or inside of minValue and maxValue to newValue (default = 0) 

Depending on the parameter &apos;insideFlag&apos;, this filter sets all values within (1) or outside (0) of the range (minValue, maxValue) to the value given by &apos;newValue&apos;. In both cases the range boundaries are not clipped and replaced. If clipping is executed outside of range, NaN and Inf values are replaced as well (floating point data objects only). This filter supports only real value data types.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>clip values of image A to newValue (default = 0) outside or inside of minValue and maxValue in image B 

Depending on the parameter &apos;insideFlag&apos;, this filter sets all values in image A depending on image B within (1) or outside (0) of the range (minValue, maxValue) to the value given by &apos;newValue&apos;. In both cases the range boundaries are not clipped and replaced. If clipping is executed outside of range, NaN and Inf values are replaced as well (floating point data objects only). This filter supports only real value data types.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>fills a ROI, which defined by a geometric primitive, of the given dataObject with a defined value

Depending on the parameter &apos;insideFlag&apos;, this filter sets all values of the dataObject depending on the geometric primitiv within (1) or outside (2) or both (3) to the value given by &apos;newValueInside&apos; and &apos;newValueOutside&apos;. The &apos;edgeFlag&apos; is currently not used but shall manage the edge handling of primitive.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BasicGenericFilters.cpp" line="-2813"/>
        <source>Tried to run generic filter engine without correct initilization of all buffers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>One kernel dimension is zero or bigger than the image size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Not enough memory to allocate linebuffer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Not enough memory to allocate invalidbuffer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Not enough memory to allocate kernel linebuffer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Not enough memory to allocate invalid linebuffer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Not enough memory to allocate output line buffer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2470"/>
        <source>Performs a median filter with kernelsize (kernelx, kernely) and pixelwise comparison of 
filtered image and original image to remove spikes according to delta value. 

At first the input image is filtered by a median filter (filter: &apos;medianFilter&apos;) using the given kernel size. The output 
image then contains the input image, where every pixel is replaced &apos;newValue&apos; if the absolute distance between the median 
filtered and the input image at the specific pixel is bigger than &apos;delta&apos;.

This filter also works inplace (same source and destination).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Performs a linear mean filter with kernelsize (kernelx, kernely) and pixelwise comparison of 
filtered image and original image to remove spikes according to delta value. 

At first the input image is filtered by a linear mean filter (filter: &apos;lowPassFilter&apos;) using the given kernel size. The output 
image then contains the input image, where every pixel is replaced &apos;newValue&apos; if the absolute distance between the median 
filtered and the input image at the specific pixel is bigger than &apos;delta&apos;. 

This filter also works inplace (same source and destination).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BasicSpecialFilters.cpp" line="-815"/>
        <source>calculates histgram of real input data object.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+822"/>
        <source>Calculates the mean value for radial circles with a given center point an a radius step size 

The radiuses are the distances from the given center point to the physical coordinates of each pixel.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
