===================
 FFTW Filters
===================

=============== ========================================================================================================
**Summary**:    :pluginsummary:`fftwFilters`
**Type**:       :plugintype:`fftwFilters`
**License**:    :pluginlicense:`fftwFilters`
**Platforms**:  Windows, Linux
**Author**:     :pluginauthor:`fftwFilters`
=============== ========================================================================================================
  
Overview
========

.. pluginsummaryextended::
    :plugin: fftwFilters

These filters are defined in the plugin:

.. pluginfilterlist::
    :plugin: fftwFilters
    :overviewonly:

Filters
==============
        
Detailed overview about all defined filters:
    
.. pluginfilterlist::
    :plugin: fftwFilters

