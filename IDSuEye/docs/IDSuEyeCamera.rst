===================
 IDS uEye
===================

=============== ========================================================================================================
**Summary**:    :pluginsummary:`IDSuEye`
**Type**:       :plugintype:`IDSuEye`
**License**:    :pluginlicense:`IDSuEye`
**Platforms**:  Windows, Linux
**Devices**:    IDS Imaging cameras
**Author**:     :pluginauthor:`IDSuEye`
=============== ========================================================================================================
 
Overview
========

.. pluginsummaryextended::
    :plugin: IDSuEye
    
Parameters
===========

An instance of this plugin has the following internal parameters:

**auto_blacklevel_enabled**: {int}, read-only
    If the camera supports an auto blacklevel correction (auto offset in addition to offset), this feature can be enabled / disabled by this parameter.
**binning**: {int}
    Horizontal and vertical binning, depending on camera ability. 104 means a 1x binning in horizontal and 4x binning in vertical direction. (values up to 1x, 2x, 3x, 4x, 5x, 6x, 8x, 12x are valid; if read-only binning is not supported; some cameras only support certain combinations of binnings.)
**bpp**: {int}
    Bitdepth of each pixel
**cam_id**: {int}, read-only
    ID of the camera
**cam_model**: {str}, read-only
    Model identifier of the attached camera
**color_mode**: {str}
    color_mode: 'gray' (default) or 'color' if color camera
**fps**: {float}, read-only
    current fps reported by camera
**frame_rate**: {float}
    frame rate in fps (will affect the allowed range of the integration_time, this frame_rate is only considered if trigger_mode == 'off'.
**gain**: {float}
    Gain (normalized value 0..1)
**gain_boost_enabled**: {int}
    enables / disables an additional analog hardware gain (gain boost). Readonly if not supported.
**gain_rgb**: {seq. of float}, read-only
    RGB-gain values (normalized value 0..1)
**integration_time**: {float}
    Exposure time of chip (in seconds).
**long_integration_time_enabled**: {int}, read-only
    If long exposure time is available, this parameter let you enable this. If this value is changed, the range and value of integration_time might change, too.
**name**: {str}, read-only
    GrabberName
**offset**: {float}
    Offset (leads to blacklevel offset) (normalized value 0..1). Readonly if not adjustable.
**pixel_clock**: {int}
    Pixel clock in MHz. If the pixel clock is too high, data packages might be lost. A change of the pixel clock might influence the exposure time.
**roi**: {int rect [x0,y0,width,height]}
    ROI (x,y,width,height) [this replaces the values x0,x1,y0,y1]
**sensor_type**: {str}, read-only
    Sensor type of the attached camera
**serial_number**: {str}, read-only
    Serial number of camera
**sizex**: {int}, read-only
    Pixelsize in x (cols)
**sizey**: {int}, read-only
    Pixelsize in y (rows)
**timeout**: {float}
    Timeout for acquiring images in seconds
**trigger_mode**: {str}
    trigger modes for starting a new image acquisition, depending on the camera the following modes are supported: 'off' (fixed frame_rate), without fixed frame_rate: 'software', 'hi_lo', 'lo_hi', 'pre_hi_lo', 'pre_lo_hi'
**x0**: {int}
    Index of left boundary pixel within ROI
**x1**: {int}
    Index of right boundary pixel within ROI
**y0**: {int}
    Index of top boundary pixel within ROI
**y1**: {int}
    Index of bottom boundary pixel within ROI

Initialization
==============
  
The following parameters are mandatory or optional for initializing an instance of this plugin:
    
    .. plugininitparams::
        :plugin: IDSuEye
        
Compilation
===========

With the sources of this plugin, the header and library files of the uEye SDK in the version denoted in the changelog are directly included. Hence, the plugin compiles as it is. 
Nevertheless, you need to have the camera drivers installed on your computer that fit to the uEye SDK of the plugin. However, you can also install the full SDK in any version
on your computer and set the CMake variable IDS_DEVELOP_DIRECTORY to the develop-subfolder of the SDK (this folder contains the include and Lib subfolder). If you indicated this,
please delete IDS_HEADER_FILE and IDS_LIBRARY in CMake and press configure again. Then, the plugin will be compiled with your individual SDK.

Please install the 32bit/64bit version of IDS uEye SDK that corresponds to your operating system, not to the type of itom. If you decide to configure the SDK installer, you don't
need to install any DirectShow or ActiveX components as well as additional drivers if you only want to use the camera with itom.

Known problems
===============

Sometimes, the camera raises an acquisition error right after a change of the trigger mode. In this case, make an idle-grab (with a possible try-except) before starting
with the right acquisition parameters.

Changelog
==========

* itom setup 2.0.0: This plugin has been compiled using the uEye SDK 4.61
* itom setup 2.1.0: This plugin has been compiled using the uEye SDK 4.61

